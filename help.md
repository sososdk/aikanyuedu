# 帮助
- 为什么我的应用不能添加在线书籍
    - 请到[爱看阅读官网](https://book.sososdk.com)或者[酷安](https://www.coolapk.com/apk/com.mdroid.read)下载最新版本
- 如何制作书源
    - 请查看[书源制作手册](https://gitee.com/sososdk/aikanyuedu/blob/master/source_manual.md)
- 如何删除书架上的书籍
    - 在书籍上左划 -> 菜单上点击删除
    - 在书籍上长按 -> 松开 -> 选中书籍 -> 点击底部删除按钮删除书籍
- 聚合搜索
    - 聚合搜索就是同时搜索多个书源, 并把搜到的书进行排序
    - 设置 -> 书源设置 -> 聚合搜索配置 -> 勾选需要聚合搜索的书源
- 如何导入书源
    - 方法一: 设置 -> 书源设置 -> 在线书源配置 -> 右上角“+”按钮 -> 网络导入-复制粘帖书源地址
    
    > 书源地址: 
    ```
    优质书源
    地址：https://gitee.com/sososdk/aikanyuedu/raw/master/backup.aks
    备用地址：https://coding.net/u/mosagi/p/aikanread_source/git/raw/master/source_lite.aks
    全部书源（包含优质书源）
    地址：https://gitee.com/sososdk/aikanyuedu/raw/master/normal.aks
    备用地址：https://coding.net/u/mosagi/p/aikanread_source/git/raw/master/source.aks
    ```

    - 方法二：群文件下载书源文件（单个书源、合集书源都有可以，看个人喜好）
    设置 -> 书源设置 -> 在线书源配置-右上角“+”按钮 -> 本地导入 -> 找到目录tencent/QQfile recv下的书源文件-导入

- 更换书源
    - 删除所有书源后再重新导入书源
- 为什么有时候书架刷新一直在转圈，要很久才能刷新完成或者是xx书更新失败？
    - 一般是书源问题，可以通过换源解决，或者忽略，直接打开你要看的书即可，照看无误
- 为什么搜索页面结果显示的更新时间、最新章节与实际这本书的最后更新时间及最新章节不一致？
    - 搜索页面的小说更新时间和最新章节仅供参考，因为是直接拉取源网站的数据，所以可能不正确，还是以实际打开后看最后的章节为准
- 怎么给书源自定义排序？
    - 在线书源设置里，按住书源那一行里的小方块按钮不放，就可以排序了
- 如何删除搜索记录？
    - 默认保留三条搜索记录，自动覆盖，无法清空
- 为什么净化替换功能没作用？
    - 净化替换功能是基于网页源码进行的, 在阅读界面看到的文字经过转码可能与源码是不一致的, 所以导致替换失败


