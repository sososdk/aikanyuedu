# 小说源制作手册
> 工欲善其事必先利其器

### 必须要掌握的知识点
- Jsoup 选择器(建议英文好的直接去看英文, 中文翻译有些错误的地方)
    - [中文文档](http://www.open-open.com/jsoup)
    - [英文文档](https://jsoup.org/apidocs/org/jsoup/select/Selector.html)
    - 工具: [xpath](https://chrome.google.com/webstore/detail/xpath-helper/hgimnogjllphhhkhlmebbmlgjoejdpjl)
- 正则表达式(做文本替换必备)
    - [文档](http://tool.oschina.net/uploads/apidocs/jquery/regexp.html)
    - 工具: [oschina](http://tool.oschina.net/regex) [chinaz](http://tool.chinaz.com/regex)

### 制作源
 - 例子:
    ```json
    [
        {
            // 小说源名字
            "name": "吾读网",
            // 书源作者
            "author": "莫哥",
            // 提示
            "tips": "30秒内只能搜索一次",
            // 版本
            "version": "1",
            // 源网站网址(PS: 最后不要加 /)
            "host": "http://www.5du5.net",
            // 源网站主页
            "indexUrl": "/",

            // 搜索页面开始
            // 搜索链接
            "searchUrl": "http://zhannei.baidu.com/cse/search?q\u003d%s\u0026p\u003d0\u0026s\u003d16043222879060885457\u0026area\u003d1",
            // 搜索参数编码, default: UTF-8
            "searchEncoding": "UTF-8",
            // POST参数
            "searchPostParams": null,
            // 搜索 user-agent(默认使用桌面)
            "searchUserAgent": null,
            // 使用 js 方式搜索(性能差, 如非必要, 不建议使用)
            "javascriptSearch": true,
            // js 获取数据延时(毫秒)
            "searchDelay": 500,
            // 搜索页面导航列表
            "searchMore": "#pageFooter a",
            // 搜索页面导航链接
            "searchMoreHref": "@a@0@abs:href",
            // 书籍列表
            "books": ".result-item",
            // 书籍名称
            "bookName": "@.result-game-item-title-link@0@",
            // 书籍链接(目前只支持指向章节列表的链接)
            "bookUrl": "@.result-game-item-title-link@0@abs:href",
            // 书籍作者
            "bookAuthor": "@.result-game-item-info-tag span@1@",
            // 书籍分类
            "bookCategory": "@.result-game-item-info-tag span@3@",
            // 书籍描述
            "bookDesc": "@.result-game-item-desc@0@",
            // 书籍图标
            "bookIcon": "@.result-game-item-pic-link-img@0@abs:src",
            // 书籍最后章节
            "bookLastChapterName": "@.result-game-item-info-tag a@0@",
            // 书籍最后更新时间
            "bookUpdateTime": "@.result-game-item-info-tag span@5@",
            // 搜索是否会直接跳转到书籍详情
            "searchToDetail": "true",
            // 搜索结束

            // 详情页开始
            // 如果要进行详情解析, detailChaptersUrl 必须有值
            "detailChaptersUrl": null,
            "detailBookIcon": null,
            // 如果要支持搜索解析详情, detailBookName 必须有值
            "detailBookName": null,
            "detailBookDesc": null,
            // 如果要支持搜索解析详情, detailBookAuthor 必须有值
            "detailBookAuthor": null,
            "detailBookCategory": null,
            "detailBookUpdateTime": null,
            "detailBookLastChapterName": null,
            // 详情页结束

            // 使用 js 方式获取章节列表(性能差, 如非必要, 不建议使用)
            "javascriptTOC": "true",
            // js 获取数据延时(毫秒)
            "TOCDelay": 300,
            // 章节列表
            "chapters": "#list li a",
            // 章节名称
            "chapterName": "@@@",
            // 章节链接
            "chapterUrl": "@@@abs:href",
            // 章节倒序
            "chaptersReverse": "true",
            // 章节重新排序
            "resortChapters": "true",
            // 删除重复章节
            "deleteDuplicatedChapters": "true",
            
            // 使用 js 方式获取章节内容(性能差, 如非必要, 不建议使用)
            "javascriptContent": "true",
            // js 获取数据延时(毫秒)
            "contentDelay": 300,
            // 文章内容
            "content": "@#content@0@",
            // 内容删除节点(使用)
            "contentRemove": "[\"a\",\".mark\"]",
            // 内容替换
            "contentReplace": "[{\"first\":\"十\\\\*\\\\*([岁|年|月|日|周|天|个|米|丈|里]*?)\",\"second\":\"十八九$1\"},{\"first\":\"你\",\"second\":\"是谁?\"}]",
        },
        ...
    ]
    ```
#### 解析方式有2种:
- 选择表达式, 使用 Jsoup 选择器得到 Element 列表
    ```
    // 搜索页面导航列表
    "searchMore": "#pageFooter a",
    ```
- 使用自定义解析方式得到一个值
    ```
    // 搜索页面导航链接
    "searchMoreHref": "@a@0@abs:href",
    ```

    > `@` 是分隔符, 使用 `@` 将 `@a@0@abs:href` 分成了3段 `a`, `0`, `abs:href`
    - `a` 是选择表达式, 将会使用 Jsoup 解析得到 Element 列表
    - `0` 是索引, 将会取上面得到的列表中的一个 Element. 索引从0开始
    - `abs:href` 是属性, 将会在上面Element中取到对应字段的值. 留空表示取文本值
    - 比较特殊的是`@@@`, 表示取文本值
- 文本获取方式
    ```
    `abs` 和 `own` 是关键字
    ```

    >
    - `@@@`: 表示取文本值
    - `@span@1@`: 表示取第二个 span 标签下的文本
    - `@span@1@:reg`: 表示取第二个 span 标签下的文本匹配正则的第一组的值
    - `@span@1@own:`: 表示取第二个 span 标签下直接包含的文本
    - `@span@1@own:reg`: 表示取第二个 span 标签下直接包含的文本匹配正则的第一组的值



